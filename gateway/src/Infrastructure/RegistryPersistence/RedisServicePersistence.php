<?php
declare(strict_types=1);

namespace App\Infrastructure\RegistryPersistence;

use App\Infrastructure\ServiceRegistryPersistenceInterface;

class RedisServicePersistence implements ServiceRegistryPersistenceInterface
{
    public function __construct()
    {
    }

    public function getAll(): array
    {
        return [];
    }

    public function getAllByKey(string $key): array
    {
        return [];
    }

    public function getOneByKey(string $key): ?string
    {
        return 'customer:8000';
    }

    public function isServiceExists(string $key): bool
    {
        return true;
    }

}