<?php
declare(strict_types=1);

namespace App\Infrastructure;

interface ServiceRegistryInterface
{
    public function getAvailableServicesList(): array;
    public function getServicesByKey(string $key): array;
    public function getFirstAvailableServiceByKey(string $key): ?string;
}