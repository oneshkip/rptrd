<?php
declare(strict_types=1);

namespace App\Infrastructure;

interface ServiceRegistryPersistenceInterface
{
    public function getAll(): array;

    public function getAllByKey(string $key): array;

    public function getOneByKey(string $key): ?string;

    public function isServiceExists(string $key): bool;
}