<?php
declare(strict_types=1);

namespace App\Application\Registry;

use App\Infrastructure\ServiceRegistryInterface;
use App\Infrastructure\ServiceRegistryPersistenceInterface;

class ServiceRegistry implements ServiceRegistryInterface
{
    public function __construct(private readonly ServiceRegistryPersistenceInterface $registryPersistence)
    {
    }

    public function getAvailableServicesList(): array
    {
    }

    public function getServicesByKey(string $key): array
    {
        // TODO: Implement getServicesByKey() method.
    }

    public function getFirstAvailableServiceByKey(string $key): ?string
    {
        if ($this->registryPersistence->isServiceExists($key)) {
            return $this->registryPersistence->getOneByKey($key);
        }

        return null;
    }


}