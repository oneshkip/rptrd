<?php

namespace App\Application\Services;

class UserService extends AbstractService
{
    public function register(array $data): array
    {
        $serviceHost = $this->getAvailableService();
        $url = $this->makeUrl('http://'.$serviceHost, '/create');

        $response = $this->makeRequest($url, $data);

        return $response;
    }
}