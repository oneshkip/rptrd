<?php

namespace App\Application\Services;

use App\Infrastructure\ServiceRegistryInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class AbstractService
{
    const SERVICE_NAME = 'USER';

    public function __construct(
        protected readonly ServiceRegistryInterface $serviceRegistry,
        protected readonly HttpClientInterface $httpClient
    )
    {
    }

    protected function getAvailableService(): ?string
    {
        return $this->serviceRegistry->getFirstAvailableServiceByKey(self::SERVICE_NAME);
    }

    public function makeRequest(string $url, array $body = [], array $params = [], string $method = 'POST')
    {

        $response = $this->httpClient->request($method, $url, ['body' => json_encode($body)]);

        return $response->toArray();
    }

    protected function makeUrl(string $host, string $endpoint)
    {
        return $host.$endpoint;
    }
}