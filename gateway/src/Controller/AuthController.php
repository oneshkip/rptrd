<?php
declare(strict_types=1);

namespace App\Controller;

use App\Application\Services\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/auth', format: 'json')]
class AuthController extends AbstractController
{
    public function __construct(private readonly UserService $userService)
    {
    }

    #[Route('/login', name: 'api_auth_login', methods: ['POST'])]
    public function login()
    {
        return $this->json(['token' => 'someRandomSting']);
    }

    #[Route('/register', name: 'api_auth_register', methods: ['POST'])]
    public function register(Request $request)
    {
        $content = json_decode($request->getContent(), true);
        $result = $this->userService->register($content);

        return $this->json($result);
    }

    public function forgotPassword()
    {

    }

    public function recoveryPassword()
    {

    }
}