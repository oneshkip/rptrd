<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(format: 'json')]
class IndexController extends AbstractController
{
    #[Route('/create', methods: ['POST'])]
    public function create(Request $request)
    {
        $content = json_decode($request->getContent(), true);
        $content['service_status'] = 'OK';
        return $this->json($content);
    }
}