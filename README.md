# Running

1. To start environment, run:```minicube start```
2. Then mount volume ```minicube mount ./:/host``` inside root of project folder

# Run helm release
```helm install <release_name> ./helm```